<?php

use App\Modules\User\Models\User;
use App\Modules\User\Models\Q;
use App\Modules\User\Models\UserQuestion;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user1 = User::create([
            'nom'           => 'TAYARI',
            'prenom'        => 'Marouéne',
            'gouvernorat'   => 'Tunis',
            'date_naissance'=> \Carbon\Carbon::now(),
            'cin'           => '11223344',
            'tel'           => '23952437',
            'provider'      => 'Facebook',
            'provider_id'   => 'FF55498xdf87qsf874df'
        ]);

        //Science
//        $user1->questions()->attach(1, ['reponse' => 1]);
//        $user1->questions()->attach(2, ['reponse' => 2]);
//        $user1->questions()->attach(3, ['reponse' => 3]);

        //Arabe
        $user1->questions()->attach(4, ['reponse' => 2]);
//        $user1->questions()->attach(5, ['reponse' => 1]);
//        $user1->questions()->attach(6, ['reponse' => 2]);

        //Math
        $user1->questions()->attach(7, ['reponse' => 3]);
//        $user1->questions()->attach(8, ['reponse' => 2]);
//        $user1->questions()->attach(8, ['reponse' => 1]);

        //Géographie
        $user1->questions()->attach(10, ['reponse' => 3]);
//        $user1->questions()->attach(11, ['reponse' => 2]);
//        $user1->questions()->attach(12, ['reponse' => 1]);


        /*********************************************************/


        $user2 = User::create([
            'nom'           => 'ANAYA',
            'prenom'        => 'Bilel',
            'gouvernorat'   => 'Tunis',
            'date_naissance'=> \Carbon\Carbon::now(),
            'cin'           => '00112233',
            'tel'           => '23952437',
            'provider'      => 'Facebook',
            'provider_id'   => 'FF55598xdf87qsf874df'
        ]);

        //Science
//        $user2->questions()->attach(1, ['reponse' => 1]);
//        $user2->questions()->attach(2, ['reponse' => 2]);
//        $user2->questions()->attach(3, ['reponse' => 3]);

        //Arabe
        $user2->questions()->attach(4, ['reponse' => 2]);
//        $user2->questions()->attach(5, ['reponse' => 1]);
//        $user2->questions()->attach(6, ['reponse' => 2]);

        //Math
        $user2->questions()->attach(7, ['reponse' => 3]);
        $user2->questions()->attach(8, ['reponse' => 2]);
//        $user2->questions()->attach(8, ['reponse' => 1]);

        //Géographie
        $user2->questions()->attach(10, ['reponse' => 3]);
        $user2->questions()->attach(11, ['reponse' => 2]);
        $user2->questions()->attach(12, ['reponse' => 1]);

        /*********************************************************/


        $user3 = User::create([
            'nom'           => 'ASKRI',
            'prenom'        => 'Ramy',
            'gouvernorat'   => 'Tunis',
            'date_naissance'=> \Carbon\Carbon::now(),
            'cin'           => '12345678',
            'tel'           => '23952437',
            'provider'      => 'Facebook',
            'provider_id'   => 'FF55598xdf87qsf874ff'
        ]);

    }


}
