<?php

use App\Modules\Question\Models\Question;
use Illuminate\Database\Seeder;

class QuestionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Question 1 : Science
        Question::create([
            'question'          => 'Q1',
            'categorie'         => 'science',
            'rang'              => 1,
            'correct_reponse'   => 1
        ]);

        // Question 2 : Science
        Question::create([
            'question'          => 'Q2',
            'categorie'         => 'science',
            'rang'              => 2,
            'correct_reponse'   => 1
        ]);

        // Question 3 : Science
        Question::create([
            'question'          => 'Q3',
            'categorie'         => 'science',
            'rang'              => 3,
            'correct_reponse'   => 1
        ]);
        //********************************************//
        // Question 1 : Arabe
        Question::create([
            'question'          => 'Q1',
            'categorie'         => 'arabe',
            'rang'              => 1,
            'correct_reponse'   => 2
        ]);

        // Question 2 : Arabe
        Question::create([
            'question'          => 'Q2',
            'categorie'         => 'arabe',
            'rang'              => 2,
            'correct_reponse'   => 1
        ]);

        // Question 3 : Arabe
        Question::create([
            'question'          => 'Q3',
            'categorie'         => 'arabe',
            'rang'              => 3,
            'correct_reponse'   => 2
        ]);
        //********************************************//
        // Question 1 : Math
        Question::create([
            'question'          => 'Q1',
            'categorie'         => 'math',
            'rang'              => 1,
            'correct_reponse'   => 2
        ]);

        // Question 2 : Math
        Question::create([
            'question'          => 'Q2',
            'categorie'         => 'math',
            'rang'              => 2,
            'correct_reponse'   => 3
        ]);

        // Question 3 : Math
        Question::create([
            'question'          => 'Q3',
            'categorie'         => 'math',
            'rang'              => 3,
            'correct_reponse'   => 1
        ]);
        //********************************************//
        // Question 1 : Géographie
        Question::create([
            'question'          => 'Q1',
            'categorie'         => 'geographie',
            'rang'              => 1,
            'correct_reponse'   => 2
        ]);

        // Question 2 : Géographie
        Question::create([
            'question'          => 'Q2',
            'categorie'         => 'geographie',
            'rang'              => 2,
            'correct_reponse'   => 3
        ]);

        // Question 3 : Géographie
        Question::create([
            'question'          => 'Q3',
            'categorie'         => 'geographie',
            'rang'              => 3,
            'correct_reponse'   => 1
        ]);
        //********************************************//



    }
}
