var myAudio = document.getElementById("myAudio");
var isPlaying = false;
console.log(myAudio);
console.log(isPlaying);
function togglePlay() {
    console.log('click');

    if (isPlaying) {
        myAudio.pause()
    } else {
        myAudio.play();
    }
};
myAudio.onplaying = function() {
    document.getElementById("btn_play_pause").innerHTML = '<img src="./gifs/pause-button.svg">';
    isPlaying = true;
};
myAudio.onpause = function() {
    document.getElementById("btn_play_pause").innerHTML = '<img src="./gifs/play-button.svg">';
    isPlaying = false;
};