<!DOCTYPE html>
<html>
@include('backOffice.inc.head', ['meta' => ''])
<body class="online-course-2x">
@include('sweet::alert')
<div class="wrapper">
    @yield('header')
    @yield('sidebar')
    @yield('content')
    @yield('footer')
</div>
@include('backOffice.inc.scripts')
</body>
</html>
