<?php

namespace App\Http\Middleware;

use App\Modules\User\Models\User;
use Closure;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Session;

class CheckUserRegistred
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $request->session()->get('user');

        if (!isset($user)) {
            Session::forget('user');
            return redirect()->route('showConnexionFacebook');
        }else{
            $userBD = User::find($user->id);
            if(!isset($userBD) or ($user->provider_id !== $userBD->provider_id)){
                Session::forget('user');
                return redirect()->route('showConnexionFacebook');
            }
        }
        return $next($request);
    }
}
