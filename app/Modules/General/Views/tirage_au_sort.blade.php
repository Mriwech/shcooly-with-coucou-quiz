<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <meta name="viewport" content="initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, width=device-width, user-scalable=no">

    <link href="https://fonts.googleapis.com/css?family=Tajawal:200,300,400,500,700,800,900&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="{{ url('css/style.css') }}" media="screen" />

    <title>Tirage au sort</title>

</head>

<body>
@include('sweet::alert')
@include('General::music_player')
<div class="container">
    @include('General::hamburger_menu')
    <div class="content" id="content_Tirage_au_sort">
        <div class="logo">
            <a href="{{ route('showMenu') }}" class="logo">
                <img src="{{ url('gifs/logo.png') }}" alt="logo" title="logo" />
            </a>
        </div>


        <div class="bloc_bravo">
            <img src="{{ url('gifs/texte_bravo.png') }}" />

            <a href="https://www.facebook.com/tropicotunisie/" class="fb_link"></a>
        </div>
    </div>
</div>
</body>
</html>
