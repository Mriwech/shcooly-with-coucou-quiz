<style>
    #btn_play_pause img{
        width:50px;
        margin-left:10px;
        margin-top:10px;
    }
</style>
<audio id="myAudio" preload="auto" src="{{ url('music/playground-music-for-kids.mp3') }}"></audio>
<a href="javascript:togglePlay()" id="btn_play_pause" style="position: absolute;z-index: 99;"></a>
<script>
    var myAudio = document.getElementById("myAudio");
    var isPlaying = false;

    if((!localStorage) || (localStorage.getItem('isPlaying') == null)){
        localStorage.setItem('isPlaying', true);
    }else{
        var isPlayin = localStorage.getItem('isPlaying');
        if(isPlayin === 'true'){
            myAudio.play();
            document.getElementById("btn_play_pause").innerHTML = '<img src="{{ url('gifs/pause-button.svg') }}">';
        }else{
            myAudio.pause();
            document.getElementById("btn_play_pause").innerHTML = '<img src="{{ url('gifs/play-button.svg') }}">';
        }
    }

    function togglePlay() {
        if (isPlaying) {
            myAudio.pause()
        } else {
            myAudio.play();
        }
    }

    myAudio.onplaying = function() {
        document.getElementById("btn_play_pause").innerHTML = '<img src="{{ url('gifs/pause-button.svg') }}">';
        isPlaying = true;
        localStorage.setItem('isPlaying', true);
    };
    myAudio.onpause = function() {
        document.getElementById("btn_play_pause").innerHTML = '<img src="{{ url('gifs/play-button.svg') }}">';
        isPlaying = false;
        localStorage.setItem('isPlaying', false);
    };
</script>
