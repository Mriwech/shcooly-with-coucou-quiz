<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <meta name="viewport" content="initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, width=device-width, user-scalable=no">

    <link href="https://fonts.googleapis.com/css?family=Tajawal:200,300,400,500,700,800,900&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="{{url('css/style.css')}}" media="screen" />
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

    <title>Matières</title>
    <link rel="stylesheet" type="text/css" href="{{ asset ('plugins') }}/sweetalert/sweetalert.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('plugins') }}/sweetalert/swal-forms.css">
    <script type="text/javascript" src="{{ asset ('plugins') }}/sweetalert/sweetalert.min.js"></script>

    <style type="text/css">
        a.disabled {
            pointer-events: auto;
            cursor: not-allowed;
            color: #00cc00;
        }
    </style>

</head>

<body class="page-matieres">
@include('sweet::alert')
@include('General::music_player')
<div class="container">
    @include('General::hamburger_menu')
    <div class="content" id="content_matieres">
        <div class="logo">
            <a href class="logo">
                <img src="{{url('gifs/logo.png')}}" alt="logo" title="logo" />
            </a>
        </div>

        <div class="bloc_matieres">
            @if(checkCategorie('arabe'))
                <a class="disabled"><i class="fa fa-check"></i>  عربية </a>
            @else
                <a href="{{route('showCheckQuizz', ['categorie' => 'arabe'])}}" >  عربية </a>
            @endif

            @if(checkCategorie('science'))
                <a class="disabled"><i class="fa fa-check"></i>علم أحياء</a>
            @else
                <a href="{{route('showCheckQuizz', ['categorie' => 'science'])}}">علم أحياء</a>
            @endif

            @if(checkCategorie('geographie'))
                <a class="disabled"><i class="fa fa-check"></i>جغرافيا</a>
            @else
                <a href="{{route('showCheckQuizz', ['categorie' => 'geographie'])}}">جغرافيا</a>
            @endif

            @if(checkCategorie('math'))
                <a class="disabled"><i class="fa fa-check"></i>حساب</a>
            @else
                <a href="{{route('showCheckQuizz', ['categorie' => 'math'])}}">حساب</a>
            @endif
        </div>
    </div>
</div>

</body>
</html>
