<?php

Route::group(['categorie' => 'General', 'middleware' => ['web'], 'namespace' => 'App\Modules\General\Controllers'], function() {

    Route::get('/menu', 'GeneralController@showMenu')->middleware('CheckUserRegistred')->name('showMenu');
    Route::get('/menu/success', 'GeneralController@showTirageAuSort')->name('showTirageAuSort')->middleware('CheckUserRegistred');

});
