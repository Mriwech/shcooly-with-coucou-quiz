<?php

namespace App\Modules\General\Controllers;

use App\Http\Controllers\Controller;



class GeneralController extends Controller
{
    public function showMenu(){
        return view('General::menu');
    }

    public function showTirageAuSort(){
        return view('General::tirage_au_sort');
    }

}
