<?php

use App\Modules\User\Models\UserQuestion;
use Illuminate\Support\Facades\Cache;

if (!function_exists('checkCategorie')) {
    /**
     * @param \App\Modules\User\Models\User $user
     * @return bool
     */
    function checkCategorie($categorie){

        $user = session()->get('user');

        if(isset($user))
            $question = UserQuestion::where('user_id', $user->id)
                ->whereHas('question', function ($query) use($categorie){
                    $query->where('categorie', $categorie);
                })->count();

        return ($question == 3) ?  true : false;

    }
}