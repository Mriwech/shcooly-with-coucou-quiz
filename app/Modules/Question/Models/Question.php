<?php

namespace App\Modules\Question\Models;

use Illuminate\Database\Eloquent\Model;

class Question extends Model {

    protected $table = 'questions';
    public $timestamps = true;
    protected $fillable = ['question', 'categorie', 'rang', 'correct_reponse'];
}
