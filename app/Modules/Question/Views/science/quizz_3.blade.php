<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <meta name="viewport" content="initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, width=device-width, user-scalable=no">

    <link href="https://fonts.googleapis.com/css?family=Tajawal:200,300,400,500,700,800,900&display=swap" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Harmattan&display=swap" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&display=swap" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Markazi+Text:400,500,600,700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="{{url('css/style.css')}}" media="screen" />

    <title>Quizz - Science 3</title>
    <link rel="stylesheet" type="text/css" href="{{ asset ('plugins') }}/sweetalert/sweetalert.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('plugins') }}/sweetalert/swal-forms.css">
    <script type="text/javascript" src="{{ asset ('plugins') }}/sweetalert/sweetalert.min.js"></script>

    <style>
        /*li{*/
        /*    font-size: 80px!Important;*/
        /*}*/
        .image_question img {
            width: auto;
        }
    </style>

</head>

<body class="page_quizz">
@include('sweet::alert')
@include('General::music_player')
<div class="container">
    @include('General::hamburger_menu')
    <div class="content" id="content_quizz">
        <div class="bloc_quizz">
            <div class="question">جسم الإنسان متكون من ؟ </div>

            <div class="theme_quizz">علم أحياء</div>

            <div class="bloc_proposition">
                <div class="content_proposition">
                    <div class="image_question">
                        <img src="{{url('gifs/science_question_3.png')}}" />
                    </div>

                    <div class="liste_proposition">
                        <ul>
                            <li>
                                <p style="direction: rtl;
                                        margin-block-start: 0px;
                                        margin-block-end: 0px;
                                        margin-inline-start: 0px;
                                        margin-inline-end: 0px;">206 عظم</p>
                            </li>
                            <li>
                                <p style="direction: rtl;
                                        margin-block-start: 0px;
                                        margin-block-end: 0px;
                                        margin-inline-start: 0px;
                                        margin-inline-end: 0px;">84 عظم</p>
                            </li>
                            <li>
                                <p style="direction: rtl;
                                        margin-block-start: 0px;
                                        margin-block-end: 0px;
                                        margin-inline-start: 0px;
                                        margin-inline-end: 0px;">5000 عظم</p>
                            </li>
                        </ul>
                    </div>

                    <div class="choix_reponse">
                        <form action="{{route('handleRepQuizz', ['categorie' => $categorie, 'id' => $id])}}" method="POST">
                            {{ csrf_field() }}
                            <button name="reponse" @if(Session::get('error') == 1) class="error_reponse" @endif value="1">1</button>
                            <button name="reponse" @if(Session::get('error') == 2) class="error_reponse" @endif value="2">2</button>
                            <button name="reponse" @if(Session::get('error') == 3) class="error_reponse" @endif value="3">3</button>
                        </form>
                    </div>
                </div>
            </div>

            <p class="btn_retour">
                <a href="{{ route('showMenu') }}">
                    <img src="{{ url('gifs/back.png') }}" width="100">
                </a>
            </p>
        </div>
    </div>
</div>
</body>
</html>