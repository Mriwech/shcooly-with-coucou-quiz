<?php

namespace App\Modules\Question\Controllers;

use App\Modules\Question\Models\Question;
use App\Modules\User\Models\UserQuestion;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Alert;
use phpDocumentor\Reflection\DocBlock\Tags\Reference\Url;

class QuestionController extends Controller
{
    /**
     * @param $categorie
     * @param string $pred
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function showCheckQuizz($categorie, $pred = 'menu'){
        if(in_array($categorie, array('science', 'arabe', 'math', 'geographie'))){

            $user = session()->get('user');

            $nbrQuestion = UserQuestion::where('user_id', $user->id)
                ->whereHas('question', function ($query) use($categorie){
                    $query->where('categorie', $categorie);
                })
                ->with('question')
                ->get()
                ->pluck('question.rang')->toArray();

//            dd($nbrQuestion);
            $nextQuestion = array_diff(array(1,2,3), $nbrQuestion);
            $nextQuestion = reset($nextQuestion);

//            dump($nbrQuestion);
//            dump($nextQuestion);
//            die();

            if(UserQuestion::where('user_id', $user->id)->count() >= 12){
                return redirect(route('showTirageAuSort'));
            }elseif(isset($nbrQuestion) and count($nbrQuestion) == 3){
                Alert::success('‫أحسنت, لقد اتممت هذه المادة بكل نجاح')->autoclose(3500);
                return redirect(route('showMenu'));
            }else{
                if(isset($nbrQuestion) and ($nextQuestion)){
                    if($pred == 'success')  Alert::success('حسن واصل')->autoclose(3500);
                    return redirect(route('showQuizz', ['categorie' => $categorie, 'id' => $nextQuestion]));
                }else{
                    return redirect(route('showMenu'));
                }
            }
        }else return redirect(route('showMenu'));
    }

    /**
     * @param $categorie
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function showQuizz($categorie, $id){
        if(in_array($categorie, array('science', 'arabe', 'math', 'geographie'))
        and ($id == 1 or $id == 2 or $id == 3)){
            return view('Question::'.$categorie.'.quizz_'.$id, ['categorie' => $categorie, 'id' => $id]);
        }else return redirect(route('showMenu'));
    }



    /**
     * @param $categorie
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function handleRepQuizz($categorie, $id, Request $request){

        if(isset($categorie) and isset($id) and in_array($categorie, array('science', 'arabe', 'math', 'geographie'))
            and ($id == 1 or $id == 2 or $id == 3)){

            if(isset($request->reponse)){
                $question = Question::where('categorie', $categorie)
                    ->where('rang', $id)
                    ->first();
                $user = session()->get('user');

                if(isset($question) and ($question->correct_reponse == $request->reponse) and (isset($user))){

                        UserQuestion::firstOrCreate([
                            'user_id'       => $user->id,
                            'question_id'   => $question->id,
                        ], ['reponse' => $request->reponse]);

                        return Redirect(route('showCheckQuizz', ['categorie' => $categorie, 'pred' => 'success']));

                }else{
                    Alert::error('إجابة خاطئة حاول مرة أخرى')->autoclose(3500);
                    return back()->with('error', $request->reponse);
                }
            }
            return Redirect(route('showCheckQuizz', $categorie));

        }else return redirect(route('showMenu'));
    }
}