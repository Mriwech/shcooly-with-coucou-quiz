<?php

Route::group(['categorie' => 'Question', 'middleware' => ['web'], 'namespace' => 'App\Modules\Question\Controllers'], function() {


    Route::get('quizz/{categorie}/show/{pred?}', 'QuestionController@showCheckQuizz')->name('showCheckQuizz')->middleware('CheckUserRegistred');
    Route::get('quizz/{categorie}/{id}', 'QuestionController@showQuizz')->name('showQuizz')->middleware('CheckUserRegistred');
    Route::POST('quizz/{categorie}/{id}/save', 'QuestionController@handleRepQuizz')->name('handleRepQuizz')->middleware('CheckUserRegistred');

});
