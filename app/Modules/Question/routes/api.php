<?php

Route::group(['categorie' => 'Question', 'middleware' => ['api'], 'namespace' => 'App\Modules\Question\Controllers'], function() {

    Route::resource('Question', 'QuestionController');

});
