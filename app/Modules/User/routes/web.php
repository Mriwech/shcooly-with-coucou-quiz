<?php

Route::group(['module' => 'User', 'middleware' => ['web'], 'namespace' => 'App\Modules\User\Controllers'], function() {

    Route::get('/', 'UserController@showConnexionFacebook')->name('showConnexionFacebook');
	Route::get('/inscription', 'UserController@showInscription')->name('showInscription');
	Route::post('/ajouter', 'UserController@handleAddUser')->name('handleAddUser');
	Route::get('/auth/redirect/{provider}', 'UserController@redirect')->name('redirect');
    Route::get('/callback/{provider}', 'UserController@callback')->name('callback');
    Route::get('/instructions', 'UserController@showInstructions')->name('showInstructions');
});
