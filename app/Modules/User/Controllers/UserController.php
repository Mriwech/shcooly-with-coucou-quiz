<?php

namespace App\Modules\User\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Laravel\Socialite\Two\InvalidStateException;
use Validator, Redirect, Response, File;
use Socialite;
use App\Modules\User\Models\User;
use Alert;

class UserController extends Controller
{


    /* affichage Page connexion */
    public function showConnexionFacebook()
    {
        Session::forget('user');
        return view("User::connexion_fb");
    }


    /* Connexion Facebook*/
    public function redirect($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    public function callback($provider, Request $request)
    {
        try {
            $getInfo = Socialite::driver($provider)->user();
            $user = User::where('provider_id', $getInfo->id)->first();
        } catch (InvalidStateException $e) {
            $getInfo = Socialite::driver($provider)->stateless()->user();
            $user = User::where('provider_id', $getInfo->id)->first();
        }

        if (!isset($user) and (isset($getInfo))){
            return redirect()->route('showInscription')
                ->with('first_name', $getInfo->first_name)
                ->with('last_name', $getInfo->last_name)
                ->with('provider_id', $getInfo->id);
        } else {
            $request->session()->put('user', $user);
            return redirect()->route('showMenu');
        }

    }

    /* End Connexion Facebook*/


    public function showInscription()
    {
        return view("User::inscription");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleAddUser(Request $request)
    {
        $request->validate([
            'nom' => 'required',
            'prenom' => 'required',
            'gouvernorat' => 'required',
            'date_naissance' => 'required',
            'cin' => 'required|digits:8',
            'tel' => 'required|digits:8',
            'provider_id' => 'required',
        ]);

        $user = User::where('cin', $request->cin)->first();
        if(isset($user)){
            return back()->withErrors(['cin' => 'هذا الرقم مستعمل'])
                ->withInput([
                        'nom' => $request->get('nom'),
                        'prenom' => $request->get('prenom'),
                        'gouvernorat' => $request->get('gouvernorat'),
                        'date_naissance' => $request->get('date_naissance'),
                        'tel' => $request->get('tel'),
                        'provider_id' => $request->get('provider_id')
                    ]);
        }


        $user = User::create([
            'nom' => $request->get('nom'),
            'prenom' => $request->get('prenom'),
            'gouvernorat' => $request->get('gouvernorat'),
            'date_naissance' => $request->get('date_naissance'),
            'cin' => $request->get('cin'),
            'tel' => $request->get('tel'),
            'provider' => "Facebook",
            'provider_id' => $request->get('provider_id')
        ]);

        $request->session()->put('user', $user);
        Alert::success('تم التسجيل بالنجاح')->autoclose(3500);
        return redirect()->route('showMenu');
    }

    public function showInstructions()
    {
        return view('User::instructions');
    }


}
