<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <meta name="viewport"
          content="initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, width=device-width, user-scalable=no">

    <link href="https://fonts.googleapis.com/css?family=Tajawal:200,300,400,500,700,800,900&display=swap"
          rel="stylesheet">

    <link rel="stylesheet" href="css/style.css" media="screen"/>

    <link rel="stylesheet" type="text/css" href="plugins/sweetalert/sweetalert.css">
    <link rel="stylesheet" type="text/css" href="plugins/sweetalert/swal-forms.css">
    <script type="text/javascript" src="plugins/sweetalert/sweetalert.min.js"></script>

    <title>Inscription</title>
</head>
<body>
@include('sweet::alert')
@include('General::music_player')
<div class="container">
    @include('General::hamburger_menu')
    <div class="content" id="content_Inscription">
        <div class="logo">
            <a href="/" class="logo">
                <img src="gifs/logo.png" alt="logo" title="logo"/>
            </a>
        </div>
        <div class="bloc_inscription">
            <form method="post" action="{{ route('handleAddUser') }}">
                {{ csrf_field() }}
                <p>
                    <input name="provider_id" type="hidden" value="{{ old('provider_id', session('provider_id')) }}">
                </p>
                <p>
                    <label>الإسم</label>
                    <input type="text" name="nom"
                           @if(!empty($errors->first('nom'))) style="border-color: red;" @endif
                           value="{{ old('nom', session('first_name')) }}"
                           placeholder="{{ $errors->first('nom') }}">
                </p>
                <p>
                    <label>اللقب</label>
                    <input type="text" name="prenom"
                           @if(!empty($errors->first('prenom'))) style="border-color: red;" @endif
                           value="{{ old('prenom', session('last_name')) }}"
                           placeholder="{{ $errors->first('prenom') }}">
                </p>

                <p>
                    <label>الولاية</label>

                    <select id="gov-select"
                            style="direction: rtl; @if(!empty($errors->first('gouvernorat'))) border-color: red; @endif"
                            name="gouvernorat" >
                        <option @if(old('gouvernorat') == '') selected @endif value="">الرجاء إختيار الولاية</option>
                        <option @if(old('gouvernorat') == 'أريانة') selected @endif value="أريانة">أريانة</option>
                        <option @if(old('gouvernorat') == 'تونس') selected @endif value="تونس">تونس</option>
                        <option @if(old('gouvernorat') == 'منوبة') selected @endif value="منوبة">منوبة</option>
                        <option @if(old('gouvernorat') == 'بن عروس') selected @endif value="بن عروس">بن عروس</option>
                        <option @if(old('gouvernorat') == 'نابل') selected @endif value="نابل">نابل</option>
                        <option @if(old('gouvernorat') == 'بنزرت') selected @endif value="بنزرت">بنزرت</option>
                        <option @if(old('gouvernorat') == 'سوسة') selected @endif value="سوسة">سوسة</option>
                        <option @if(old('gouvernorat') == 'زغوان') selected @endif value="زغوان">زغوان</option>
                        <option @if(old('gouvernorat') == 'المنستير') selected @endif value="المنستير">المنستير</option>
                        <option @if(old('gouvernorat') == 'المهدية') selected @endif value=" المهدية"> المهدية</option>
                        <option @if(old('gouvernorat') == 'صفاقس') selected @endif value="صفاقس">صفاقس</option>
                        <option @if(old('gouvernorat') == 'باجة') selected @endif value="باجة">باجة</option>
                        <option @if(old('gouvernorat') == 'جندوبة') selected @endif value="جندوبة">جندوبة</option>
                        <option @if(old('gouvernorat') == 'الكاف') selected @endif value="الكاف">الكاف</option>
                        <option @if(old('gouvernorat') == 'سليانة') selected @endif value="سليانة">سليانة</option>
                        <option @if(old('gouvernorat') == 'القيروان') selected @endif value="القيروان">القيروان</option>
                        <option @if(old('gouvernorat') == 'سيدي بوزيد') selected @endif value="سيدي بوزيد">سيدي بوزيد</option>
                        <option @if(old('gouvernorat') == 'القصرين') selected @endif value="القصرين">القصرين</option>
                        <option @if(old('gouvernorat') == 'قابس') selected @endif value="قابس">قابس</option>
                        <option @if(old('gouvernorat') == 'مدنين') selected @endif value="مدنين">مدنين</option>
                        <option @if(old('gouvernorat') == 'قفصة') selected @endif value="قفصة">قفصة</option>
                        <option @if(old('gouvernorat') == 'توزر') selected @endif value="توزر">توزر</option>
                        <option @if(old('gouvernorat') == 'تطاوين') selected @endif value="تطاوين">تطاوين</option>
                        <option @if(old('gouvernorat') == 'قبلي') selected @endif value="قبلي">قبلي</option>
                    </select>
                </p>

                <p>
                    <label>تاريخ الميلاد</label>
                    <input type="date" name="date_naissance" value="{{ old('date_naissance') }}"
                   @if(!empty($errors->first('date_naissance'))) style="border-color: red;" @endif>
                </p>

                <p>
                    <label>بطاقة تعريف وطنية</label>
                    <input type="number" name="cin" max="99999999"
                           @if(!empty($errors->first('cin'))) style="border-color: red;" @endif
                           value="{{ old('cin') }}" placeholder="{{ $errors->first('cin') }}">
                </p>

                <p>
                    <label>الهاتف</label>
                    <input type="tel" name="tel" maxlength="8"
                           @if(!empty($errors->first('tel'))) style="border-color: red;" @endif
                           value="{{ old('tel') }}" placeholder="{{ $errors->first('tel') }}">
                </p>

                <p class="submit_inscription">
                    <button type="submit" value=""><img src="gifs/btn_inscription.png"/></button>
                </p>
            </form>
        </div>
    </div>
</div>

</body>
</html>
