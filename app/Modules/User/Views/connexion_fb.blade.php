<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <meta name="viewport"
          content="initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, width=device-width, user-scalable=no">

    <link href="https://fonts.googleapis.com/css?family=Tajawal:200,300,400,500,700,800,900&display=swap"
          rel="stylesheet">

    <link rel="stylesheet" href="{{ url('css/style.css') }}" media="screen"/>
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
          integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
            integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
            crossorigin="anonymous"></script>
    <title>Connexion FB</title>
</head>

<body>
@include('General::music_player')
<div class="container">
    @include('General::hamburger_menu')
    <div class="content" id="content_connexion_fb">
        <div class="logo">
            <a href="{{ route('showConnexionFacebook') }}" class="logo">
                <img src="{{ url('gifs/logo.png') }}" alt="logo" title="logo"/>
            </a>
        </div>

        <div class="bloc_connexion_fb">
            <div class="btn_fb">
                <span><img src="{{ url('gifs/fb.png') }}"/></span>
            </div>

            <div class="txt_connexion_fb">
                <img src="{{ url('gifs/connecti.png') }}"/>
                <img src="{{ url('gifs/charek_maana.png') }}"/>
            </div>
        </div>
        <div class="btn_elaab">
            <button><a href="{{ route('redirect', 'facebook')}}"><img src="{{ url('gifs/btn_el3ab.png') }}"/></a>
            </button>
        </div>
        <div class="clear_both"></div>
        <div id="btn_popup">
            <a href data-toggle="modal" data-target=".bd-example-modal-lg">
                <img src="{{ url('gifs/cartable-x10.png') }}" style="width: 100%;">
            </a>
        </div>
    </div>
</div>

<!-- Large modal -->
<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content" style="background-color: #413d4400;
                                        background-clip: padding-box;
                                        border: 0px solid rgba(0,0,0,.2);
                                        border-radius: 0">
            <img src="{{ url('gifs/back.png') }}" class="close" data-dismiss="modal" aria-hidden="true" width="50">
{{--            <i class="fa fa-times close" data-dismiss="modal" aria-hidden="true"--}}
{{--               style="color: #fff!Important;font-weight: lighter;text-shadow: none;"></i>--}}
            <img src="{{ url('gifs/Game-Instructions.png') }}" style="width: 100%">
        </div>
    </div>
</div>
</body>
</html>