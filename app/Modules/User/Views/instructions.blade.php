<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <meta name="viewport" content="initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, width=device-width, user-scalable=no">

    <link href="https://fonts.googleapis.com/css?family=Tajawal:200,300,400,500,700,800,900&display=swap" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Harmattan&display=swap" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&display=swap" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Markazi+Text:400,500,600,700&display=swap" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Mirza:400,500,600,700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="{{ url('css/style.css') }}" media="screen" />

    <title>Instructions</title>

</head>

<body class="page_instruction">
@include('General::music_player')
<div class="container">
    @include('General::hamburger_menu')
    <div class="content" id="content_instruction">

        <div class="bloc_instruction">
            <div class="question">كيفاش نلعب ؟</div>
            <div class="theme_quizz">التعليمات</div>
            <div class="bloc_proposition">
                <div class="content_proposition">
                    <p>راجع دروسك وحضر للعودة المدرسية مع كوكو ! جاوب بالصحيح على الأسئلة في المواد الكل و أدخل في القرعة باش تربح </p>
                    <a href="{{ route('showConnexionFacebook') }}"><img src="{{ url('gifs/cartables.png') }}" id="img_cartable" /></a>
                </div>
            </div>
        </div>
        <p class="btn_retour">
            <a href="{{ route('showConnexionFacebook') }}">
                <img src="{{ url('gifs/back.png') }}" width="100">
            </a>
        </p>
    </div>
</div>
</body>
</html>
