<?php

namespace App\Modules\User\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable {
    use Notifiable;

    protected $table = 'users';
    public $timestamps = true;

    protected $fillable = ['nom', 'prenom','gouvernorat','date_naissance', 'cin', 'tel', 'provider', 'provider_id'];

    public function questions(){
        return $this->belongsToMany('App\Modules\Question\Models\Question','user_questions','user_id','question_id')
            ->withPivot('reponse')
            ->withTimestamps();
    }

    public function validQuestions(){
        return UserQuestion::where('user_id', $this->id)->count();
    }
}
