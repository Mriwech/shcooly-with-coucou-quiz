<?php

namespace App\Modules\User\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class UserQuestion extends Pivot {

    protected $table = 'user_questions';
    public $timestamps = true;

    protected $fillable = ['user_id', 'question_id' ,'reponse'];

    public function user(){
        return $this->hasOne('App\Modules\User\Models\User', 'id', 'user_id');
    }

    public function question(){
        return $this->hasOne('App\Modules\Question\Models\Question', 'id', 'question_id');
    }
}
