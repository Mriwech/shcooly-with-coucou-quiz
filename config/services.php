<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
	
	
	'facebook' => [
        'client_id' => env('FACEBOOK_CLIENT_ID', '653695285137690'),
        'client_secret' => env('FACEBOOK_CLIENT_SECRET', 'efb1474835100b6d3471d349333f6685'),
        'redirect' => env('CALLBACK_URL_FACEBOOK', 'http://localhost/shcooly-with-coucou-quiz/public/callback/facebook'),
    ],



];
